const steps = document.querySelectorAll(".indecater__num");
const formSteps = document.querySelectorAll(".step");
const form = document.getElementById("form");
const pricingCards = document.querySelectorAll(".plan__card");
const addonCards = document.querySelectorAll(".addon__card");
const switchPlanBtn = document.getElementById("change-plan");

let selectedPlan = {};

const selectedAddons = function () {
  let addonArray = [];
  addonCards.forEach((card) => {
    let price = card.querySelector(".sbscription__price").textContent;
    let name = card.querySelector(".card__name").textContent;
    let planDuration = card.querySelector(".sbscription__duration").textContent;
    if (card.classList.contains("selected")) {
      addonArray.push({
        price,
        name,
        planDuration,
      });
    }
  });
  return addonArray;
};

const monthlyPlanPrices = [9, 12, 15];
const yearlyPlanPrices = [90, 120, 150];
const monthlyAddonsPrice = [1, 2, 2];
const yearlyAddonsPrice = [10, 20, 20];

const setPricing = function (card, price, duration) {
  card.forEach((card, i) => {
    card.querySelector(".sbscription__price").textContent = `${price[i]}`;
    card.querySelector(".sbscription__duration").textContent = `${duration}`;
  });
};

setPricing(pricingCards, monthlyPlanPrices, "m");
setPricing(addonCards, monthlyAddonsPrice, "m");

const nextButton = document.getElementById("next-button");
const prevButton = document.getElementById("prev-button");

let currentStep = 0;

const selectPlanError = function (text) {
  document.getElementById("select-plan-error").textContent = text;
};

nextButton.addEventListener("click", (e) => {
  e.preventDefault();
  if (currentStep === 0) {
    if (!validateForm()) return;
    currentStep++;
    showCurrentStep(currentStep);
  } else if (currentStep === 1) {
    if (Object.entries(selectedPlan).length === 0) {
      return selectPlanError("Please select a plan");
    }
    currentStep++;
    showCurrentStep(currentStep);
  } else if (currentStep === 2) {
    renderTotalAmount();
    currentStep++;
    showCurrentStep(currentStep);
  } else if (currentStep === 3) {
    currentStep++;
    showCurrentStep(currentStep);
  } else return;
});

prevButton.addEventListener("click", function (e) {
  e.preventDefault();
  currentStep--;
  return showCurrentStep(currentStep);
});

const showCurrentStep = function (step) {
  selectPlanError("");
  console.log(step);

  if (step < steps.length) {
    for (let i = 0; i < steps.length; i++) {
      steps[i].classList.remove("active");
    }
    steps[step].classList.add("active");
  }

  if (step < formSteps.length) {
    if (step === 0) {
      prevButton.classList.add("hidden");
      prevButton.setAttribute("disabled", "");
    } else if (step === 4) {
      nextButton.parentElement.classList.add("hidden");
    } else {
      prevButton.classList.remove("hidden");
      prevButton.removeAttribute("disabled");
    }
    step === 3
      ? (nextButton.textContent = "Confirm")
      : (nextButton.textContent = "Next step");

    for (let i = 0; i < formSteps.length; i++) {
      formSteps[i].classList.remove("active");
    }
    formSteps[step].classList.add("active");
  }
};
showCurrentStep(currentStep);

const showFormError = function (input, warningText) {
  input.classList.add("error");
  input.parentElement.querySelector(".warning").textContent = warningText;
};

const hideFormError = function (input) {
  input.classList.remove("error");
  input.parentElement.querySelector(".warning").textContent = "";
};

const formInputs = form.querySelectorAll("input");

const validateForm = function () {
  formInputs.forEach((input) => {
    console.log(input);
    if (input.name === "userName") {
      return input.value.length === 0
        ? showFormError(input, "Enter your name")
        : hideFormError(input);
    }
    if (input.name === "email") {
      const emailRegExp = new RegExp("[a-z0-9]+@[a-z]+.[a-z]{2,3}");
      return input.value.length === 0
        ? showFormError(input, "Enter email")
        : !emailRegExp.test(input.value)
        ? showFormError(input, "Enter valid email")
        : hideFormError(input);
    }
    if (input.name === "phone") {
      return input.value.length === 0
        ? showFormError(input, "Enter your mobile number")
        : hideFormError(input);
    }
  });
  return form.checkValidity();
};

const toggleSwitch = document.getElementById("toggle");
const yearlyBenefits = document.querySelectorAll(".yearly__benefit");
const month = document.getElementById("monthly");
const year = document.getElementById("yearly");

toggleSwitch.addEventListener("click", function (e) {
  selectPlanError("");
  const toggleSwitch = e.target.parentElement;
  pricingCards.forEach((card) => card.classList.remove("selected"));
  selectedPlan = {};

  toggleSwitch.classList.toggle("active");

  if (toggleSwitch.classList.contains("active")) {
    yearlyBenefits.forEach((item) => item.classList.add("show"));
    setPricing(pricingCards, yearlyPlanPrices, "y");
    setPricing(addonCards, yearlyAddonsPrice, "y");
    year.classList.add("selected__plan");
    month.classList.remove("selected__plan");
  } else {
    setPricing(pricingCards, monthlyPlanPrices, "m");
    setPricing(addonCards, monthlyAddonsPrice, "m");
    yearlyBenefits.forEach((item) => item.classList.remove("show"));
    month.classList.add("selected__plan");
    year.classList.remove("selected__plan");
  }
});

pricingCards.forEach((card) => {
  card.addEventListener("click", (e) => {
    selectPlanError("");
    let target = e.currentTarget;
    console.log(target);
    pricingCards.forEach((card) => card.classList.remove("selected"));
    target.classList.add("selected");

    let planName = target.querySelector(".card__name").textContent;
    let planPrice = target.querySelector(".sbscription__price").textContent;
    let planDuration = target.querySelector(
      ".sbscription__duration"
    ).textContent;
    console.log(planDuration, planName, planPrice);

    return (selectedPlan = { planName, planPrice, planDuration });
  });
});

addonCards.forEach((card) => {
  card.addEventListener("click", function (e) {
    let target = e.currentTarget;
    let checkbox = target.querySelector(".checkbox");
    target.classList.toggle("selected");

    if (target.classList.contains("selected")) {
      return (checkbox.checked = true);
    } else {
      return (checkbox.checked = false);
    }
  });
});

const renderTotalAmount = function () {
  let totalAmount = 0;
  const planDuration = selectedPlan.planDuration === "m" ? "Monthly" : "Yearly";
  const plan = document.getElementById("selected-plan");
  const addsOnList = document.getElementById("selected-addon");
  const total = document.getElementById("total");

  total.innerHTML = "";
  addsOnList.innerHTML = "";
  plan.innerHTML = "";

  let planName = document.createElement("p");
  planName.textContent = selectedPlan.planName;

  let dur = document.createElement("p");
  console.log(dur);
  dur.textContent = `(${planDuration})`;

  let planPrice = document.createElement("p");
  planPrice.textContent = `$${selectedPlan.planPrice}/${selectedPlan.planDuration}`;
  plan.appendChild(planName);
  plan.appendChild(dur);
  plan.appendChild(planPrice);

  totalAmount += parseInt(selectedPlan.planPrice);

  selectedAddons().forEach((item) => {
    let listItem = document.createElement("li");
    let addOnName = document.createElement("p");
    addOnName.textContent = item.name;
    let addOnprice = document.createElement("p");
    addOnprice.textContent = `+$${item.price}/${item.planDuration}`;

    listItem.appendChild(addOnName);
    listItem.appendChild(addOnprice);
    addsOnList.appendChild(listItem);

    totalAmount += parseInt(item.price);
  });

  total.innerHTML = `<span>Total(
    per ${planDuration.slice(0, -2).toLocaleLowerCase()}) </span> 
      <span> $${totalAmount}/${selectedPlan.planDuration}</span>`;
};

switchPlanBtn.addEventListener("click", function () {
  currentStep = 0;
  showCurrentStep(currentStep);
});

console.log(0);
window.addEventListener("load", function () {
  console.log(1);
  console.log(document.querySelector(".button__container"));
  document.querySelector(".button__container").classList.remove("hidden");
});
